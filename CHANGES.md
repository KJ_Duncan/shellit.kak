### Shellit Version 3.1 - 02/03/2021 ###

+ build.gradle.kts package version bumps
+ ParseCliArgs.kt new interface ContinuumCliArgs
+ new gradle task uberJar replaces third party dependency shadowJar


### Shellit Version 3.0 - 09/04/2020 ###

+ shellit.kak package restructure
+ Continuum package added (a semantic-versioner)
+ Continuum.kt, ContinuumDT.kt, Result.kt, Scope.kt added with test suite 
+ Usage.kt updated to display long and short help messages -h, --h
+ Scope.kt is the applications interface composition
+ Shellit.kt private mainContinuum method added
+ ParseCliArgs.kt continuumCliArgs method added
+ interfaces Shellit and Debugit renamed to Shell and Debug
+ interfaces ParseCliArgs, ProcessBuilderDebug, Continuum now contravariant (consumer in) 
+ CommandLine.kt renamed to CommandDT.kt
+ test suite refactored for assertj package dependency 
+ dependency removed [kotest-BETA1](https://github.com/kotest/kotest) (aka kotlintest)
+ version bump [gradle wrapper 6.3](https://docs.gradle.org/6.3/release-notes.html)
+ updates to docstrings, UML diagrams, coverage, javadoc.


### Shellit Version 2.0 - 01/03/2020 ###

+ CommandLine helper value runShellProcess
+ CommandTemplates update from sh -c now call /bin/sh -c
+ processBuilderCommands -> runShellProcess -> shellProcessTemplate
+ MessageClient improved encapsulation
+ ExecCli properties added '/' and 'bin'
+ Text named to Usage, message upgraded and namespaced
+ three tests add to CommandTemplateTest
+ version bumps; dokka 0.10.1, gradle 6.2.1, graalvm 20.0.0.r11-grl
+ resources/uml/* non-functional requirement longer packaged in jar
+ updates to docstrings, UML diagrams, coverage, javadoc.  


### Shellit Version 1.0 - 12/02/2020 ###

+ renamed package to shellit.kak
+ cli commands are now `sh`
+ removed dead code in ExecCli: JavaCli & KotlinCli
+ usage text and readme updated
+ updates to docstrings, UML diagrams, coverage, javadoc.  


---


### Bashit Version 5.0 - 05/02/2020 ###

+ reflection and coroutines removed as it provided a no functional requirement
+ build.gradle.kts kotlin compiler options added -no-stdlib -no-reflect
+ Timeout sealed class and tests added for future cli extendability
+ refactored interface ParseCliArgs with no implicit side effects to .jar and .kts calls 
+ refactored MessageClient onDebugMarkup, onBashItException dead code removed
+ console logging via println only for graalvm native-image --no-fallback image compliance
+ usage text and readme updated
+ updates to docstrings, UML diagrams, coverage, javadoc.  


### Bashit Version 4.0 - 19/01/2020 ###

+ Console.outputException function added for terminal debugging exceptions
+ processBuilderOnDebug implements outputException
+ processBuilderRedirectOutput refactored to correctly output stderr to temp file
+ build.gradle.kts refactored `application` plugin removed `maven-publish` plugin added
+ new directory add resources/uml contains puml files
+ reformatted UML diagrams to puml/png
+ examples added to the Text.USAGE help output
+ docs/javadoc updated
+ docs/coverage updated
+ bashit.run optional implementation
+ pre-built bashit-4.0.jar & bashit.run available for download  


### Bashit Version 3.0 - 04/01/2020 ###

+ CommandLine parameters are now all type safe through inline class
+ processBuilderOnDebug method refactored to log a native process
+ main method `when` condition flattened 
+ two Boolean functions added to Console: isSuccess, isFailure
+ renamed TimerWrapper to DurationTimer
+ removed file CommandLineTestMock as it duplicated CommandLineTest
+ package tests reduced from 137 to 121, BashIt.kt lines 179 & 190 require commenting out to pass package tests  
+ uml diagrams now reside in resources/img/
+ small refactoring to methods and functions 
+ Updates to docstrings, UML diagrams, coverage, javadoc.  


### Bashit Version 2.0 - 25/12/2019 ###

+ Separation of bashit debug process and a restriction on the domain from type Any? to Any for interfaces. 
+ Two new interfaces: DebugIt which provides the environment for ProcessBuilderDebug.  
+ Refactor of the main methods parsing of command line args while the mainDebug has improvements in encapsulation.  
+ The processBuilderRedirectOutput method now also redirects stderr to tempfile on a failed process.  
+ Updates to docstrings, UML diagrams, coverage, javadoc.  
+ -debug flag reworked for console/terminal command line use: java -jar bashit.jar -debug <binary|jar>  

