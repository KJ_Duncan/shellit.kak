package org.kjd.shellit.kak.util

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

internal class TimeoutTest {

  @Test
  fun `Timeout members are not null`() {
    assertNotNull(Timeout.TWENTY)
    assertNotNull(Timeout.FORTY)
    assertNotNull(Timeout.SIXTY)
  }

  @Test
  fun `Timeout members get seconds is exactly 20,40,60 Long`() {
    assertEquals(20L, Timeout.TWENTY.Seconds)
    assertEquals(40L, Timeout.FORTY.Seconds)
    assertEquals(60L, Timeout.SIXTY.Seconds)
  }
}
