package org.kjd.shellit.kak.util

import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

internal class DurationTimerTest {

  @Test
  fun `DurationTimer should not be null`() {
    assertNotNull(DurationTimer)
  }

  @Test
  fun `DurationTimer stop should not be null`() {
    DurationTimer.start()
    assertNotNull(DurationTimer.stop())
  }
}
