package org.kjd.shellit.kak

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.kjd.shellit.kak.client.CliArgs
import org.kjd.shellit.kak.continuum.ContinuumCmd

internal class ParseCliArgsTest {

  private object MockObjParseCliArgs :
    ParseCliArgsImpl<MockObjParseCliArgs> {}

  private object MockObjContinuumCliArgs :
    ContinuumCliArgsImpl<MockObjContinuumCliArgs> {}

  private val arrayOfStrings: Array<String> = arrayOf<String>(
    "this",
    "should",
    "be",
    "a",
    "single",
    "sentence"
  )

  private val arrayWithJar: Array<String> = arrayOf<String>(
    "kak-client",
    "mailit.jar",
    "-p"
  )

  private val arrayWithKts: Array<String> = arrayOf<String>(
    "kak-client",
    "mailit.kts",
    "-p"
  )

  private val arrayWithFlag: Array<String> = arrayOf<String>(
    "kak-client",
    "-i",
    "mailit",
    "-p"
  )

  @Test
  fun `CliAndLogImpl getKakouneArgs function should return all members of an array given on assignment`() {
    MockObjParseCliArgs.run {
      assertEquals(CliArgs(arrayOfStrings.drop(1).toList()), kakouneCliArgs()(arrayOfStrings).cliArgs)
    }
  }

  @Test
  fun `CliAndLogImpl getKakouneArgs function should return a size of 5`() {
    MockObjParseCliArgs.run {
      assertEquals(5, kakouneCliArgs()(arrayOfStrings).cliArgs.args.size)
    }
  }

  @Test
  fun `prefixCliArgsList method should not add java -jar to the front of list`() {
    MockObjParseCliArgs.run {
      assertNotEquals(CliArgs(listOf("java", "-jar", "mailit.jar", "-p")), kakouneCliArgs()(arrayWithJar).cliArgs)
    }
  }

  @Test
  fun `prefixCliArgsList method should not add kotlinc -script to the front of list`() {
    MockObjParseCliArgs.run {
      assertNotEquals(CliArgs(listOf("kotlinc", "-script", "mailit.kts", "-p")), kakouneCliArgs()(arrayWithKts).cliArgs)
    }
  }

  @Test
  fun `kakouneCliFlagArgs should be mailit -p`() {
    MockObjParseCliArgs.run {
      assertEquals(CliArgs(arrayWithFlag.drop(2).toList()), kakouneCliFlagArgs()(arrayWithFlag).cliArgs)
    }
  }

  @Nested
  inner class ContinuumCliArgsTest {

    private val arrayContinuumFqNameAddMajor: Array<String> = arrayOf<String>(
      "continuum",
      "add",
      "major",
      "a description"
    )

    private val arrayContinuumAddMajor: Array<String> = arrayOf<String>(
      "cm",
      "add",
      "major",
      "a description"
    )

    private val arrayContinuumAddMinor: Array<String> = arrayOf<String>(
      "cm",
      "add",
      "minor",
      "a description"
    )

    private val arrayContinuumAddPatch: Array<String> = arrayOf<String>(
      "cm",
      "add",
      "patch",
      "a description"
    )

    private val arrayContinuumChangeLog: Array<String> = arrayOf<String>(
      "cm",
      "changelog"
    )

    private val arrayContinuumCurrent: Array<String> = arrayOf<String>(
      "cm",
      "current"
    )

    private val arrayContinuumRelease: Array<String> = arrayOf<String>(
      "cm",
      "release"
    )

    private val arrayContinuumError: Array<String> = arrayOf<String>(
      "cm",
      "unknownCommand"
    )

    @Test
    fun `continuumCliArgs continuum fully quailfied name with add major returns ContinuumCmd add`() {
      MockObjContinuumCliArgs.run {
        assertThat(continuumCliArgs()(arrayContinuumFqNameAddMajor)).isExactlyInstanceOf(ContinuumCmd.add::class.java)
      }
    }

    @Test
    fun `continuumCliArgs continuum add major returns ContinuumCmd add`() {
      MockObjContinuumCliArgs.run {
        assertThat(continuumCliArgs()(arrayContinuumAddMajor)).isExactlyInstanceOf(ContinuumCmd.add::class.java)
      }
    }

    @Test
    fun `continuumCliArgs continuum add minor returns ContinuumCmd add`() {
      MockObjContinuumCliArgs.run {
        assertThat(continuumCliArgs()(arrayContinuumAddMinor)).isExactlyInstanceOf(ContinuumCmd.add::class.java)
      }
    }

    @Test
    fun `continuumCliArgs continuum add patch returns ContinuumCmd add`() {
      MockObjContinuumCliArgs.run {
        assertThat(continuumCliArgs()(arrayContinuumAddPatch)).isExactlyInstanceOf(ContinuumCmd.add::class.java)
      }
    }

    @Test
    fun `continuumCliArgs continuum changelog returns ContinuumCmd changelog`() {
      MockObjContinuumCliArgs.run {
        assertThat(continuumCliArgs()(arrayContinuumChangeLog)).isExactlyInstanceOf(ContinuumCmd.changelog::class.java)
      }
    }

    @Test
    fun `continuumCliArgs continuum release returns ContinuumCmd release`() {
      MockObjContinuumCliArgs.run {
        assertThat(continuumCliArgs()(arrayContinuumRelease)).isExactlyInstanceOf(ContinuumCmd.release::class.java)
      }
    }

    @Test
    fun `continuumCliArgs continuum current returns ContinuumCmd current`() {
      MockObjContinuumCliArgs.run {
        assertThat(continuumCliArgs()(arrayContinuumCurrent)).isExactlyInstanceOf(ContinuumCmd.current::class.java)
      }
    }

    @Test
    fun `continuumCliArgs continuum unknown command returns ContinuumCmd error`() {
      MockObjContinuumCliArgs.run {
        assertThat(continuumCliArgs()(arrayContinuumError)).isExactlyInstanceOf(ContinuumCmd.error::class.java)
      }
    }
  }
}
