package org.kjd.shellit.kak.closure

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.kjd.shellit.kak.continuum.ContinuumException

@Suppress("DIVISION_BY_ZERO")
internal class ResultTest {

  private val success = Result.Success(value = "true")
  private val failure = Result.Failure(reason = ContinuumException(""))

  @Test
  fun `assert Success and Failure are not null`() {
    assertNotNull(success)
    assertNotNull(failure)
  }

  @Test
  fun `Success value equals "true"`() {
    assertEquals("true", success.value)
  }

  @Test
  fun `Failure reason is a NoSuchElementException`() {
    assertThat(failure.reason).isInstanceOf(ContinuumException::class.java)
  }

  @Test
  fun `resultFrom does not throw Exception`() {
    assertDoesNotThrow { resultFrom { Exception() } }
  }

  @Test
  fun `mapFailure returns a NullPointerException on 1 div 0`() {
    assertThrows<NullPointerException> { resultFrom { (1 / 0) }.mapFailure { it.cause!! } }
  }

  /* TEST: the fold function
   *  Success.fold({  }, {  })
   *  Failure.fold({  }, {  })
  @author https://github.com/kittinunf/Result/blob/master/result/src/main/kotlin/com/github/kittinunf/result/Result.kt#L90
  inline fun <T, E, W> Result<T, E>.fold(success: (T) -> W, failure: (E) -> W): W = when (this) {
    is Success -> success(this.value)
    is Failure -> failure(this.reason)
  }
   */
}
