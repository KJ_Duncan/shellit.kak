package org.kjd.shellit.kak.closure

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

@Suppress("DIVISION_BY_ZERO")
internal class ConsoleTest {

  @Test
  fun `Console println string parameter should be of type Unit`() {
    assertSame(kotlin.io.println(), Console.println{})
    assertSame(Unit, Console.println { "1: println to console a String" })
  }

  @Test
  fun `Console print string should be of type Unit`() {
    assertSame(Unit, Console.print { "2: print to console a String" })
  }

  @Test
  fun `Console isSuccess value should be false`() {
    assertFalse(Console.isSuccess("Output of org.kjd.shellit.kak.closure.ConsoleTest.println") {
      10 / 0
    })
  }

  @Test
  fun `Console isSuccess value should be true`() {
    assertTrue(Console.isSuccess("Output of org.kjd.shellit.kak.closure.ConsoleTest.println") {
      10 / 10
    })
  }

  @Test
  fun `Console isFailure value should be true`() {
    assertTrue(Console.isFailure("Output to Console.println") {
      10 / 0
    })
  }

  @Test
  fun `Console isFailure value should be false`() {
    assertFalse(Console.isFailure("Output to Console.println") {
      10 / 10
    })
  }

  @Test
  fun `Console outputException op() should not throw Exception`() {
    assertDoesNotThrow { Console.outputException { "op() -> R" } }
  }
}

