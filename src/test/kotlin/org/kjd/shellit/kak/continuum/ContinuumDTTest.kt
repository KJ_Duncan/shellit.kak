package org.kjd.shellit.kak.continuum

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class ContinuumDTTest {

  private val changes = Changes(ReleaseType.patch, Description("because we care"))

  @Test
  fun `Changes is not null`() {
    assertNotNull(changes)
  }

  @Test
  fun `ContinuumCmd is not null`() {
    assertNotNull(ContinuumCmd.error)
  }

  @Test
  fun `ContinuumCmd has type add and name is 'add'`() {
    assertEquals("add", ContinuumCmd.add.name)
  }

  @Test
  fun `ReleaseType is not null`() {
    assertNotNull(ReleaseType.error)
  }

  @Test
  fun `ReleaseType has type major and name is 'major'`() {
    assertEquals("major", ReleaseType.major.name)
  }

  @Test
  fun `Changes release type is patch`() {
    assertSame(ReleaseType.patch, changes.type)
  }

  @Test
  fun `Changes description equals 'because we care'`() {
    assertEquals("because we care", changes.description.value)
  }

  @Test
  fun `Changes toString equals '{description because we care, type patch}'`() {
    assertEquals("""{"type": "patch", "description": "because we care"}""", changes.toString())
  }
}
