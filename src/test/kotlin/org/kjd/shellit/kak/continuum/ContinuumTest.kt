package org.kjd.shellit.kak.continuum

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import org.junit.jupiter.api.assertDoesNotThrow
import org.kjd.shellit.kak.closure.Result
import org.kjd.shellit.kak.closure.map
import org.kjd.shellit.kak.closure.mapFailure
import java.io.File

internal class ContinuumTest {

  private object ObjContinuum : ContinuumImpl<ObjContinuum> {}

  // private val file: String = """[{"type": "major", "description": "major update run from continuum test"}, {"type": "minor", "description": "minor update run from continuum test"}, {"type": "patch", "description": "patch update run from continuum test"}]"""

  /*
  @BeforeAll
  fun `setup remove any failing test directories`() {
    val parent = File(".changes")
    val child = File(".changes/next-release")
    if (child.exists()) child.deleteRecursively()
    else parent.deleteRecursively()
  }
  */

  @AfterAll
  fun `clean up changes directory`() {
    val parent = File(".changes")
    val child = File(".changes/next-release")
    if (child.exists()) child.deleteRecursively()
    else parent.deleteRecursively()
  }

  @Test
  fun `continuum is not null`() {
    assertNotNull(ObjContinuum)
  }

  @Nested
  inner class CurrentVersion {

    @Test
    fun `currentVersion on empty directory should throw ContinuumException`() {
      assertDoesNotThrow {
        ObjContinuum.run {
          currentVersion().mapFailure {
            assertThat(it)
              .isInstanceOf(ContinuumException::class.java)
              .hasMessage("ERROR: current method was unable to retrieve a release file please check the directory .changes")
            it
          }
        }
      }
    }

    @Test
    fun `currentVersion on empty directory should be equal to Result Failure ContinuumException`() {
      ObjContinuum.run {
        assertThat(currentVersion()).isInstanceOf(Result.Failure::class.java)
      }
    }
  }

  @Nested
  @TestMethodOrder(OrderAnnotation::class)
  inner class AddChangePatch {
    private val changePatch = Changes(
      ReleaseType.patch,
      Description("patch update run from continuum test")
    )

    @Test
    @Order(1)
    fun `addChange creates a json file prefixed with patch and returns true`() {
      ObjContinuum.run { addChange(changePatch) }.map { assertEquals(true, it) }
    }

    @Test
    @Order(2)
    fun `addChange created directories changes and next-release`() {
      val parent = File(".changes")
      val child = File(".changes", "next-release")
      assertThat(parent).exists()
      assertThat(child).exists()
    }

    @Test
    @Order(3)
    fun `addChange created one json file with prefix name patch`() {
      val child = File(".changes", "next-release")
      val files = child.listFiles()
      val file = files.filter { it.name.startsWith("patch") }
      assertTrue(files.size > 0)
      assertEquals("json", file[0].name.substringAfterLast('.'))
    }
  }

  @Nested
  @TestMethodOrder(OrderAnnotation::class)
  inner class AddChangeMinor {
    private val changeMinor = Changes(
      ReleaseType.minor,
      Description("minor update run from continuum test")
    )

    @Test
    @Order(1)
    fun `addChange creates a json file prefixed with minor and returns true`() {
      ObjContinuum.run { addChange(changeMinor) }.map { assertEquals(true, it) }
    }

    @Test
    @Order(2)
    fun `addChange created one json file with prefix name minor`() {
      val dir = File(".changes/next-release")
      val files = dir.listFiles()
      val file = files.filter { it.name.startsWith("minor") }
      assertTrue(files.size > 0)
      assertEquals("json", file[0].name.substringAfterLast('.'))
    }
  }

  @Nested
  @TestMethodOrder(OrderAnnotation::class)
  inner class AddChangeMajor {
    private val changeMajor = Changes(
      ReleaseType.major,
      Description("major update run from continuum test")
    )

    @Test
    @Order(1)
    fun `addChange creates a json file prefixed with major and returns true`() {
      ObjContinuum.run { addChange(changeMajor) }.map { assertEquals(true, it) }
    }

    @Test
    @Order(2)
    fun `addChange created one json file with prefix name major`() {
      val dir = File(".changes/next-release")
      val files = dir.listFiles()
      val file = files.filter { it.name.startsWith("major") }
      assertTrue(files.size > 0)
      assertEquals("json", file[0].name.substringAfterLast('.'))
    }
  }

  @Nested
  @TestMethodOrder(OrderAnnotation::class)
  inner class Release {

    @Test
    @Order(1)
    fun `directories changes and next-release exist`() {
      val parent = File(".changes")
      val child = File(".changes", "next-release")
      assertTrue(parent.exists())
      assertTrue(child.exists())
    }

    @Test
    @Order(2)
    fun `release does not throw Exceptions`() {
      assertDoesNotThrow { ObjContinuum.run { release() } }
    }

    @Test
    @Order(3)
    fun `next-release directory does not exist`() {
      val child = File(".changes", "next-release")
      assertFalse(child.exists())
    }

    @Test
    @Order(4)
    fun `changes directory contains json file(s) name greater than 0 0 0`() {
      val parent = File(".changes")
      val files = parent.walk().maxDepth(1).filter { it.isFile }
      assertTrue(files.count() > 0)
    }
  }

  @Nested
  inner class ChangeLog {

    @Test
    fun `changelog does not throw Exception`() {
      assertDoesNotThrow { ObjContinuum.run { changeLog() } }
    }

    @Test
    fun `changelog returns a string that contains (Changelog|major|minor|patch)`() {
      ObjContinuum.run { changeLog() }.map {
        assertThat(it).containsPattern("\\b(Changelog|major|minor|patch)\\b")
        println(it)
      }
    }
  }
}
