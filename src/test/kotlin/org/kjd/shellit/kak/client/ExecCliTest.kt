package org.kjd.shellit.kak.client

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

internal class ExecCliTest {

  @Nested
  inner class KakCliTest {
    @Test
    fun `kak cli object is not null`() {
      assertNotNull(KakCli)
    }

    @Test
    fun `kak cli kak equals kak`() {
      assertEquals("kak", KakCli.KAK)
    }

    @Test
    fun `kak cli edit equals edit!`() {
      assertEquals("edit!", KakCli.EDIT)
    }

    @Test
    fun `kak cli list of flags -l, -c, -p equals the same KakCli flags`() {
      assertSame(
        "-l, -c, -p",
        "${KakCli.FLAG_L}, ${KakCli.FLAG_C}, ${KakCli.FLAG_P}"
      )
    }

    @Test
    fun `kak cli hook, global, echo equals the same KakCli const val`() {
      assertSame(
        "hook, global, echo",
        "${KakCli.HOOK}, ${KakCli.GLOBAL}, ${KakCli.ECHO}"
      )
    }

    @Test
    fun `kak cli list of flags -existing -markup, -debug equals the same KakCli flags`() {
      assertSame(
        "-existing, -markup, -debug",
        "${KakCli.FLAG_EXISTING}, ${KakCli.FLAG_MARKUP}, ${KakCli.FLAG_DEBUG}"
      )
    }

    @Test
    fun `kak cli -always, -once, equals the same KakCli const val`() {
      assertSame(
        "-always, -once",
        "${KakCli.ALWAYS}, ${KakCli.ONCE}"
      )
    }

    @Test
    fun `kak cli Normal_Idle equals NormalIdle`() {
      assertEquals("NormalIdle", KakCli.NORMAL_IDLE)
    }
  }

  @Nested
  inner class ShellCliTest {
    @Test
    fun `sh cli object exists`() {
      assertNotNull(ShellCli)
    }

    @Test
    fun `sh cli sh equals sh`() {
      assertEquals("sh", ShellCli.SHELL)
    }

    @Test
    fun `sh cli flag c equals -c`() {
      assertEquals("-c", ShellCli.FLAG_C)
    }

    @Test
    fun `sh cli echo equals echo`() {
      assertEquals("echo", ShellCli.ECHO)
    }

    @Test
    fun `sh cli pipe equals |`() {
      assertEquals("|", ShellCli.PIPE)
    }
  }
}
