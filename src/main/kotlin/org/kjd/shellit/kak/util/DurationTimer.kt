package org.kjd.shellit.kak.util

import org.kjd.shellit.kak.closure.Console

/**
 * DurationTimer a stopwatch. The start and stop methods log with [Console.println].
 *
 * [DurationTimer UML](/shellit.kak/src/main/resources/img/DurationTimer.png)
 */
object DurationTimer {
  private var started: Long = 0L

  fun start(): Unit {
    started = System.currentTimeMillis()
  }

  fun stop(): Unit {
    Console.println("Duration [ms]: ${(System.currentTimeMillis() - started)}")
  }
}
