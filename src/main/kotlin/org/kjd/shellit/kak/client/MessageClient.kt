package org.kjd.shellit.kak.client

/**
 * OnSuccess overrides the kotlin.toString method and injects the parameter message.
 *
 * [Message Client UML](/shellit.kak/src/main/resources/img/MessageClient.png)
 *
 * `echo -markup '{Information}Shell onload message'`
 *
 * @param message overrides [kotlin.toString] method.
 * @see [org.kjd.shellit.kak.client.markupSuccessTemplate]
 */
class OnSuccess private constructor(private val message: String) {
  override fun toString(): String = markupSuccessTemplate(message)

  companion object {
    fun markup(message: String): OnSuccess =
      OnSuccess(message)
  }
}

/**
 * OnFailure overrides the kotlin.toString method and injects the parameter message.
 *
 * [Message Client UML](/shellit.kak/src/main/resources/img/MessageClient.png)
 *
 * `echo -markup '{Error}Shell onload message'`
 *
 * @param message overrides [kotlin.toString] method.
 * @see [org.kjd.shellit.kak.client.markupFailureTemplate]
 */
class OnFailure private constructor(private val message: String) {
  override fun toString(): String = markupFailureTemplate(message)

  companion object {
    fun markup(message: String): OnFailure =
      OnFailure(message)
  }
}
