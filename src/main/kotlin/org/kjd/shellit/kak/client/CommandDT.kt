package org.kjd.shellit.kak.client

import java.io.File

/** CliArgs a parsed list of command line [args]. */
inline class CliArgs(val args: List<String>)

/**
 * KakClient is the `$kak_session` used when shellit is run inside Kakoune client.
 *
 * `define-command -params .. shellit %{ nop %sh{ java -jar <YOUR-PATH-TO>/shellit.kak-<version>.jar $kak_session $@ } }`
 *
 * `define-command -params .. shellit %{ nop %sh{ shellit.run $kak_session $@ } }`
 */
inline class KakClient(val client: String)

/** Commands a list of terminal [cmds] used to interact with the Kakoune client. */
inline class Commands(val cmds: List<String>)

/** Message a releaseType safe data structure that provides a [markup] message for the Kakoune client. */
inline class Message(val markup: String)

/**
 * CommandLine maintains the state of the application. Also, provides helper
 * string templates as an immutable [Commands] for [java.lang.ProcessBuilder].
 *
 * Access CommandList via public methods: [CommandLine UML](/shellit.kak/src/main/resources/img/CommandDT.png)
 *
 * @property runShellProcess `/bin/sh -c $@`
 * @see [org.kjd.shellit.kak.client.shellProcessTemplate]
 *
 * @property kakConnectProcess `/bin/sh -c echo 'edit! -existing file' | kak -p kakClient`
 * @see [org.kjd.shellit.kak.client.connectProcessTemplate]
 *
 * @property kakMessageMarkup `/bin/sh -c echo 'echo -markup {Information|Error}Shell onload message' | kak -p kakClient`
 * @see [org.kjd.shellit.kak.client.markupMessageTemplate]
 *
 * @param kakClient a type safe value
 * @see [org.kjd.shellit.kak.client.KakClient]
 *
 * @param cliArgs a type safe value
 * @see [org.kjd.shellit.kak.client.CliArgs]
 *
 * @param message a type safe value
 * @see [org.kjd.shellit.kak.client.Message]
 *
 * @param lazyFile temp file prefix "kak-shellit-XXXXX" in users local `echo $TMPDIR`
 * @throws java.io.IOException
 */
data class CommandLine(
  val kakClient: KakClient,
  val cliArgs: CliArgs,
  var message: Message = Message("placeholder"),
  val lazyFile: Lazy<File> = lazy {
    kotlin.runCatching {
      createTempFile("kak-shellit-")
    }.getOrThrow()
  }
) {

  val runShellProcess: Lazy<Commands> = lazy {
    shellProcessTemplate(cliArgs)
  }

  val kakConnectProcess: Lazy<Commands> = lazy {
    connectProcessTemplate(kakClient, lazyFile.value)
  }

  val kakMessageMarkup: Lazy<Commands> = lazy {
    markupMessageTemplate(kakClient, message)
  }

  override fun toString(): String = ""
}
