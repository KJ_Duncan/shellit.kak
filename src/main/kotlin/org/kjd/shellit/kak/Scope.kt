package org.kjd.shellit.kak

import org.kjd.shellit.kak.continuum.ContinuumImpl
import org.kjd.shellit.kak.io.ProcessBuilderDebugImpl
import org.kjd.shellit.kak.io.ProcessBuilderShellImpl

/** Shell is the scope of the application [Shell UML](/shellit.kak/src/main/resources/img/Shell.png) */
internal interface Shell : ParseCliArgsImpl<Shell>,
  ProcessBuilderShellImpl<Shell>

/** Debug is the scope of the application [Debug UML](/shellit.kak/src/main/resources/img/Debug.png) */
internal interface Debug : ParseCliArgsImpl<Debug>,
  ProcessBuilderDebugImpl<Debug>

/** Variant is the scope of the application [Variant UML](/shellit.kak/src/main/resources/img/Variant.png) */
internal interface Variant : ContinuumCliArgsImpl<Variant>,
  ContinuumImpl<Variant>
