package org.kjd.shellit.kak.closure

/**
 * Reader monad provides a closure (left) and suspends the computation
 * on the closure until required (right). No computation occurs until the
 * call to the termination method Reader.runReader(closure).
 *
 * [Reader UML](/shellit.kak/src/main/resources/img/Reader.png)
 *
 * Technically the Reader is a Typeclass as it wraps an object.
 * In this project I use the class within a function: ENV.run -> R.
 * It's behaviour was easier to reason with as a closure over the outer ENV object.
 *
 * @author Castillo, J 2017, Kotlin Dependency Injection with the Reader Monad,
 *         jorgecastillo.dev, 31 March 2019, viewed on 19 Nov 2019,
 *         https://jorgecastillo.dev/kotlin-dependency-injection-with-the-reader-monad
 *
 * @param runReader (R) -> A
 */
class Reader<R, out A>(val runReader: (R) -> A) {
  /**
   * map transformation function from A to B and returns the result.
   *
   * @param f (A) -> B
   * @return Reader<R, B>
   */
  fun <B> map(f: (A) -> B): Reader<R, B> =
    Reader { r -> f(runReader(r)) }

  /**
   * flatMap transformation function from A to Reader<B> the
   * result is a wrapped value.
   *
   * @param f (A) -> Reader<R, B>
   * @return Reader<R, B>
   */
  fun <B> flatMap(f: (A) -> Reader<R, B>): Reader<R, B> =
    Reader { r -> f(runReader(r)).runReader(r) }

  companion object {
    /**
     * Reader.pure wraps a value into the Reader closure.
     *
     * @param a A is any releaseType
     * @return Reader<R, A>
     */
    fun <R, A> pure(a: A): Reader<R, A> =
      Reader { a }

    /**
     * Reader.ask runs the Reader in current closure.
     *
     * @return Reader<R, R>
     */
    fun <R> ask(): Reader<R, R> =
      Reader { r -> r }
  }
}
