package org.kjd.shellit.kak

import org.kjd.shellit.kak.client.CliArgs
import org.kjd.shellit.kak.client.CommandLine
import org.kjd.shellit.kak.client.KakClient
import org.kjd.shellit.kak.continuum.ContinuumCmd

/**
 * ParseCliArgs an interface framework for parsing Kakoune command line arguments.
 * @see [ParseCliArgsImpl] for method implementation
 */
interface ParseCliArgs<in ENV : Any> {
  fun ENV.kakouneCliArgs(): (Array<String>) -> CommandLine
  fun ENV.kakouneCliFlagArgs(): (Array<String>) -> CommandLine
}

/** ParseCliArgsImpl implements [ParseCliArgs]
 * [ParseCliArgs UML](/shellit.kak/src/main/resources/img/ParseCliArgs.png) */
interface ParseCliArgsImpl<in ENV : Any> : ParseCliArgs<ENV> {
  /**
   * kakouneCliFlagArgs a lambda of kotlin.Array<String> which process
   * Kakoune command line arguments given with flag -i or -d.
   *
   * @receiver [ENV] is the applications scope
   * @return [CommandLine] with [KakClient] and [CliArgs] properties set.
   */
  override fun ENV.kakouneCliFlagArgs(): (Array<String>) -> CommandLine =
    { array ->
      CommandLine(
        kakClient = KakClient(array.first()),
        cliArgs = CliArgs(array.drop(2))
      )
    }

  /**
   * kakouneCliArgs a lambda that takes kotlin.Array as its positional argument
   * and returns a CommandLine data class.
   *
   * @receiver [ENV] is the applications scope
   * @return [CommandLine] with [KakClient] and [CliArgs] properties set.
   */
  override fun ENV.kakouneCliArgs(): (Array<String>) -> CommandLine =
    { array ->
      CommandLine(
        kakClient = KakClient(array.first()),
        cliArgs = CliArgs(array.drop(1))
      )
    }
}

/**
 * ContinuumCliArgs an interface framework for parsing command line arguments.
 * @see [ContinuumCliArgsImpl] for method implementation
 */
interface ContinuumCliArgs<in ENV : Any> {
  fun ENV.continuumCliArgs(): (Array<String>) -> ContinuumCmd
}

/**
 * continuumCliArgs a lambda that takes kotlin.Array<String> as its positional
 * argument and returns a ContinuumCmd enum class.
 *
 * @receiver [ENV] is the applications scope
 * @return [ContinuumCmd] [org.kjd.shellit.kak.continuum.ContinuumCmd]
 */
interface ContinuumCliArgsImpl<in ENV : Any> : ContinuumCliArgs<ENV> {
  override fun ENV.continuumCliArgs(): (Array<String>) -> ContinuumCmd =
    { array ->
      when {
        array[1].contentEquals(ContinuumCmd.add.name)       -> ContinuumCmd.add
        array[1].contentEquals(ContinuumCmd.changelog.name) -> ContinuumCmd.changelog
        array[1].contentEquals(ContinuumCmd.current.name)   -> ContinuumCmd.current
        array[1].contentEquals(ContinuumCmd.release.name)   -> ContinuumCmd.release
        else                                                -> ContinuumCmd.error
      }
    }
}
