package org.kjd.shellit.kak.continuum

import org.kjd.shellit.kak.closure.Result
import org.kjd.shellit.kak.closure.mapFailure
import org.kjd.shellit.kak.closure.resultFrom
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.random.Random

/**
 * Continuum interface provides extension functions on an [ENV].
 * @see [ContinuumImpl] for method implementation
 */
interface Continuum<in ENV : Any> {
  fun ENV.addChange(changes: Changes): Result<Boolean, Throwable>

  fun ENV.release(): Result<Boolean, Throwable>

  fun ENV.changeLog(): Result<String, Throwable>

  fun ENV.currentVersion(): Result<String, Throwable>
}

/** [ContinuumImpl] implements [Continuum] and is a framework around [java.io.File].
 * [ContinuumImpl UML](/shellit.kak/src/main/resources/img/Continuum.png) */
interface ContinuumImpl<in ENV : Any> : Continuum<ENV> {
  /**
   * addChange takes Changes as a positional argument and creates a json file.
   * Prefixed with a release type name the file is a chronicle of changes.
   *
   * @param changes [org.kjd.shellit.kak.continuum.Changes]
   * @return [org.kjd.shellit.kak.closure.Result] Result<Boolean, Throwable>
   */
  override fun ENV.addChange(changes: Changes): Result<Boolean, Throwable> =
    resultFrom {
      if (noReleaseDir()()) nextReleaseDir()().mkdirs()

      /* 1999/12/31-23:59:00 */
      val dateTime: () -> String = { val df = SimpleDateFormat("yyyyMMdd-HHmmss"); df.format(Date()) }
      val fileName = "${changes.type.name}-${dateTime()}-${Random.nextInt(0, 1000)}.json"
      val changeFile = File(nextReleaseDir()(), fileName)

      if (newFile()(changeFile)) changeFile.writeText(changes.toString())
      fileOK()(changeFile)
    }

  /**
   * release takes all the created json change files from the `next-release` directory
   * and concatenates them into a single json file for that release.
   *
   * @return [org.kjd.shellit.kak.closure.Result] Result<Boolean, Throwable>
   */
  override fun ENV.release(): Result<Boolean, Throwable> =
    resultFrom {
      val temp = copyNextReleaseFilesToTempFile()(nextReleaseFiles()())
      val releaseFile = versionFileName()(currentReleaseFiles()())
      if (copyTempFileToNewReleaseFile()(temp, releaseFile)) nextReleaseDir()().deleteRecursively()
      fileOK()(releaseFile)
    }

  /**
   * changeLog prints the latest release file to console.
   * Use command line redirection `>` CHANGELOG.md to capture stdout.
   *
   * @return [org.kjd.shellit.kak.closure.Result] Result<String, Throwable>
   */
  override fun ENV.changeLog(): Result<String, Throwable> =
    resultFrom {
      val sb = StringBuilder()

      sb.appendln("# Changelog")
      sb.appendln("Note: version releases in the 0.x.y range may introduce breaking changes.\n")

      currentReleaseFiles()().forEach { file ->
        val version = file.name.takeWhile { it != 'v' }.substringBeforeLast('.')
        val date = file.nameWithoutExtension.dropWhile { it != 'v' }.substringAfter('v')
        sb.appendln("## $version - $date\n")

        /* readText returns a single string for files < 2GB */
        val changes = file.readText().parseText()()
        sb.appendln("$changes\n")
      }
      sb.toString()
    }

  /**
   * currentVersion prints the latest semantic version number to stdout.
   *
   * @return [org.kjd.shellit.kak.closure.Result] Result<String, Throwable>
   */
  override fun ENV.currentVersion(): Result<String, Throwable> =
    resultFrom { currentReleaseFiles()().first().nameWithoutExtension }
      .mapFailure {
        return Result.Failure(ContinuumException("ERROR: current method was unable to retrieve a release file please check the directory .changes"))
      }

  // -------------------------------------- Helper Functions -------------------------------------- //

  private fun anyFiles(): (Sequence<File>) -> Boolean = { it.any() }

  private fun fileOK(): (File) -> Boolean = { it.exists() && it.length() > 0L }

  private fun noReleaseDir(): () -> Boolean = { nextReleaseDir()().exists().not() }

  /* file is readable=true, writable=true, ownerOnly=true */
  private fun newFile(): (File) -> Boolean = { file ->
    file.createNewFile()
    file.setReadable(true, true)
    file.setWritable(true, true)
  }

  private fun nextReleaseDir(): () -> File = { File(".changes${File.separator}next-release") }

  /* file name prefix: major-, minor-, patch- */
  private fun releaseType(): () -> String =
    { nextReleaseFiles()().first().name.substringBefore("-") }

  /* walk the next-release directory and sort files where: major > minor > patch */
  private fun nextReleaseFiles(): () -> Sequence<File> =
    { nextReleaseDir()().walk().filter { it.isFile }.sorted() }

  /* walk the .changes directory and sort files where: 1.0.0 > 0.1.0 > 0.0.1 */
  private fun currentReleaseFiles(): () -> Sequence<File> =
    {
      nextReleaseDir()().parentFile.walk().maxDepth(1).filter { it.isFile }
        .sortedByDescending {
          it.name
            .takeWhile { c: Char -> c != 'v' }
            .substringBeforeLast('.')
            .split('.')
            .joinToString(separator = "", prefix = "", postfix = "")
            .toFloat()
        }
    }

  /* [major, minor, patch, v1999-12-31, json] = [0, 1, 2, ...] */
  private fun version(): (File) -> String = { file ->
    val version = file.name.split('.')

    val major = version[0].toInt()
    val minor = version[1].toInt()
    val patch = version[2].toInt()

    /* 1999-12-31 */
    val date: () -> String = { val df = SimpleDateFormat("yyyy-MM-dd"); df.format(Date()) }

    val incrementRelease: (String) -> String = { name ->
      when (name.toLowerCase()) {
        ReleaseType.patch.name -> {
          "$major.$minor.${patch + 1}.v${date()}"
        }
        ReleaseType.minor.name -> {
          "$major.${minor + 1}.0.v${date()}"
        }
        ReleaseType.major.name -> {
          "${major + 1}.0.0.v${date()}"
        }
        else -> throw ContinuumException("ERROR: increment release: unable to increment to the next release number please check .changes/next-release.")
      }
    }
    incrementRelease(releaseType()())
  }

  /* initial version = 0.0.0.json */
  private fun versionFileName(): (Sequence<File>) -> File = { versions ->
    if (anyFiles()(versions)) {
      File(nextReleaseDir()().parentFile, "${version()(versions.first())}.json")
    } else {
      File(nextReleaseDir()().parentFile, "${version()(File("0.0.0"))}.json")
    }
  }

  /* jsonArray builder [{"type": "patch", "description": "a short description"}] */
  private fun copyNextReleaseFilesToTempFile(): (Sequence<File>) -> File = { releases ->
    if (anyFiles()(releases)) {
      val size = releases.count()
      val length = size - 1

      releases.foldIndexed(createTempFile("kak-continuum-", ".json")) { index, acc, file ->
        when {
          size == 1      -> acc.appendText(file.readLines().joinToString("", "[", "]"))
          index == 0     -> acc.appendText(file.readLines().joinToString(" ", "[", ", "))
          index < length -> acc.appendText(file.readLines().joinToString(" ", postfix = ", "))
          else           -> acc.appendText(file.readLines().joinToString(" ", postfix = "]"))
        }
        acc
      }
    } else throw ContinuumException("ERROR: copying next release files to temp file: no files found in the directory .changes/next-release")
  }

  private fun copyTempFileToNewReleaseFile(): (File, File) -> Boolean = { temp, release ->
    if (newFile()(release)) {
      temp.copyRecursively(release, overwrite = true) { _, ioException -> throw ioException }
      temp.delete()
    } else throw ContinuumException("ERROR: copying temp to new file inorder to create a new release file in the directory .changes")
  }

  private fun String.parseText(): () -> String = {
    replace("([{|\"|}])".toRegex(), "")
      .replace("(type:\\s|\\stype:\\s)".toRegex(), "- ")
      .replace(",\\sdescription:\\s".toRegex(), ": ")
      .replace(",-(?=\\s\\w+:)".toRegex(), "\n-")
      .removeSurrounding("[", "]")
  }
}
