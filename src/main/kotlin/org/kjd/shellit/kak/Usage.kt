package org.kjd.shellit.kak

/**
 * Usage provides a console help message.
 *
 * [Usage UML](/shellit.kak/src/main/resources/img/Usage.png)
 *
 * @author The Open Group 2018, 'Utility Conventions',
 *         12.1 Utility Argument Syntax, The Open Group Base Specifications Issue 7,
 *         2018 edition IEEE Std 1003.1-2017 (Revision of IEEE Std 1003.1-2008),
 *         viewed 26 Feb 2020, https://pubs.opengroup.org/onlinepubs/9699919799/
 *
 * @author The Open Group 2018, '14.2 Utility Description Defaults',
 *         Introduction, The Open Group Base Specifications Issue 7,
 *         2018 edition IEEE Std 1003.1-2017 (Revision of IEEE Std 1003.1-2008),
 *         viewed 26 Feb 2020, https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap01.html
 *
 * @author Gomis, R 2019, 'Semversioner', Bitbucket Pipelines, Pipes Tooling,
 *         Atlassian and others, Apache 2.0 licensed, viewed 13 Mar 2020,
 *         https://bitbucket.org/bitbucketpipelines/semversioner/src/master/
 *
 * @author Lacon, O & Fortune, T 2020, 'keep a changelog', version 1.0.0,
 *         viewed on 13 Mar 2020, https://keepachangelog.com/en/1.0.0/
 *
 * @author Preston-Werner, T 2013, 'Semantic Versioning 2.0.0', viewed on
 *         13 Mar 2020, https://semver.org/
 */
object Usage {
  const val MESSAGE_LONG: String = """
NAME

   shellit - a Kakoune command line gateway
   ${'$'} /bin/sh -c $@

SYNOPSIS

   shellit [-d|-h|--h|-i] [binary|jar|script]...[flags...][args...]

   shellit [continuum|cm] [-h] [option] [...]

DESCRIPTION

   Shell a Kakoune command line gateway calls /bin/sh -c to
   easily invoke a binary, jar, or any script/executable without
   leaving the kakoune command line. With automated temp file
   setup and teardown on stdin/out/err. Default invocation opens
   the temporary file in current kakoune client :buffer-next.
   Kakoune exit :q triggers the temporary file deletion, invoke
   kakoune's :write command to save the buffers output to a file
   name and destination of your choosing.

   Continuum allows you to manage semantic versioning and changelog
   generation files. Continuum takes all the created JSON change
   files from the `next-release` directory and concatenates them into
   a single JSON file for that release (e.g `1.12.0.v1999-12-31.json`).

OPTIONS

   -d
       terminal debugging of the java.lang.Process

   -h
       prints a shorter help message and exit

   --h
       print a long help message and exit

   -i
       ignore stdout temp file in Kakoune client

   binary|jar|script

       is on your PATH | CLASSPATH | `pwd`

   flags

       native to the binary | jar | script

   args

       native to the binary | jar | script

   continuum|cm

       allows you to manage semantic versioning and changelog generation files.

     OPTIONS

        add
            major|minor|patch "a description requires double quotes, it appears in the change log".

        release

            takes all the created JSON change files from the `next-release` directory
            and concatenates them into a single JSON file for that release.

        current

            prints the latest semantic version number to stdout.

        changelog

            prints the latest release file to stdout.
            Use command line redirection `>` CHANGELOG.md to capture stdout.

STDOUT

   :buffer-next

       to view temp file in Kakoune editor

   -i
       flag provides success or failure markup in
       kakoune on the first command call. Failure
       propagation with command substitution is
       unrecorded.

STDERR

   :buffer *debug*

       the *debug* buffer is available in the Kakoune editor

OUTPUT FILES

   $ ls ${'$'}TMPDIR | grep kak-shellit

   deletion of temporary files on the Kakoune :quit command.

APPLICATION USAGE

   define-command -docstring "shellit /bin/sh -c $@" \
       -params .. shellit %{ nop %sh{ shellit.kak ${'$'}kak_session ${'$'}{@} } }

   alias global sh shellit

EXAMPLES

   :sh sfmt -s %val{buffile}
   :sh shellcheck -fgcc -Cnever %val{buffile}

   :sh -i ls -l > file.txt
   :sh cat file.txt
   :sh "pgrep Polar | wc -l | awk '{print $1}'"
   :sh python3 --help && echo && pip3 --help

   :sh perl --help
   :sh shellit.run --help

   :sh brew info shellcheck
   :sh -i brew home shellcheck

   :sh howdoi -c -n 5 catch exception kotlin
   :sh curl https://cheat.sh/:intro

   :ansi-render
   :sh -i open https://github.com/eraserhd/kak-ansi
   :sh -i xdg-open https://github.com/eraserhd/kak-ansi

   :sh shellit.kak continuum add minor "reducing errors with type safety"
   :sh shellit.kak continuum release
   :sh shellit.kak continuum changelog > CHANGELOG.md

FUTURE DIRECTIONS

   Shellit design is of stateless interfaces that provide an in memory syntax tree.
   Interface composition allows scoping this global tree. The aim will be to unify
   the repositories below into the Kotlin language and offer the user the ability
   to interact with a scoped module in the Kakoune editor. The two projects are
   open source written in bash and shell scripts.

   git-flow - https://github.com/nvie/gitflow
   kakmerge - https://github.com/lenormf/kakmerge

SEE ALSO

   shellit.kak README.md at https://tinyurl.com/wp65lto

CHANGE HISTORY

   shellit.kak CHANGES.md at https://tinyurl.com/yx5cozhy
"""

// ------------------------------------------------------------------------------------------------ //

  const val MESSAGE_SHORT = """
   shellit [-d|-h|-i] [binary|jar|script]...[flags...][args...]

   shellit [continuum|cm] [-h] [option] [...]

OPTIONS

   -d terminal debugging of the java.lang.Process

   -h print this help message and exit

   -i ignore stdout temp file in Kakoune client

   binary|jar|script is on your PATH | CLASSPATH | `pwd`

   flags are native to the binary | jar | script

   args are native to the binary | jar | script

CONTINUUM

   alias cm continuum

   Prefix with the `shellit` command:
   :sh shellit.kak continuum [option] [...]

   Expands to posix shell:
   /bin/sh shellit.kak continuum [option] [...]

   continuum add major/minor/patch "a description requires double quotes, it appears in the change log"
   continuum release
   continuum current
   continuum changelog > CHANGELOG.md
  """

// ------------------------------------------------------------------------------------------------ //
// FAQ: Is there a suggested regular expression (RegEx) to check a SemVer string?
//      https://regex101.com/r/Ly7O1x/3/ ; https://regex101.com/r/vkijKf/1/
// ------------------------------------------------------------------------------------------------ //
  const val MESSAGE_CONTINUUM: String = """
Continuum allows you to manage semantic versioning and changelog generation files.

At any given time, the `.changes/` directory looks like:
    .changes
    |
    └── next-release
        ├── major-DATE-TIME-RANDOM.json
        ├── minor-20200323-205319-374.json
        └── patch-20200323-205319-854.json
    ├── 1.1.0.vYEAR-MONTH-DAY.json
    ├── 1.1.1.v2020-05-25.json
    ├── 1.1.2.v2020-06-30.json

Continuum takes all the created JSON change files from the `next-release` directory and
concatenates them into a single JSON file for that release (e.g `1.12.0.v1999-12-31.json`).

Usage
=====

   shellit [continuum|cm] [-h] [option] [...]

   alias cm continuum

   Prefix with the `shellit` command:
   :sh shellit.kak continuum [option] [...]

   Expands to posix shell:
   /bin/sh shellit.kak continuum [option] [...]

   ${'$'} continuum add major "a description requires double quotes, it appears in the change log"
   ${'$'} continuum add minor "a description requires double quotes, it appears in the change log"
   ${'$'} continuum add patch "a description requires double quotes, it appears in the change log"
   ${'$'} continuum release
   ${'$'} continuum current
   ${'$'} continuum changelog > CHANGELOG.md

Source adapted: <https://bitbucket.org/bitbucketpipelines/semversioner/src/master/>
"""

// ------------------------------------------------------------------------------------------------ //

  const val MESSAGE_CONTINUUM_SHORT: String = """
   shellit [continuum|cm] [-h] [option] [...]

   alias cm continuum

   Prefix with the `shellit` command:
   :sh shellit.kak continuum [option] [...]

   Expands to posix shell:
   /bin/sh shellit.kak continuum [option] [...]

   continuum add major|minor|patch "a description requires double quotes, it appears in the change log"
   continuum release
   continuum current
   continuum changelog > CHANGELOG.md
  """
}
